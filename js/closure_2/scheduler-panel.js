goog.provide('demo.redux2.scheduler');

demo.redux2.scheduler = function () {

};

demo.redux2.scheduler.prototype.init = function () {
    function render() {
        var currentState = window.store.getState();
        $('scheduler-position-title').innerText=currentState.position.position;
    }

    render();
    window.store.subscribe(render);
};