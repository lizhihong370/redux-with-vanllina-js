goog.provide('demo.redux2.attendance');

demo.redux2.attendance = function () {

};


demo.redux2.attendance.prototype.reducer = function () {
    return function status(state, action) {

        if (typeof state === 'undefined') {
            return {status: 'At Work'}
        }

        switch (action.type) {
            case 'CHANGE_STATUS':
                return Object.assign({}, state, {status: action.payload.status});
            default:
                return state;
        }
    }
};

demo.redux2.attendance.prototype.init = function () {

    function changeStatus() {
        let newStatus = $('attendance-select').value;
        console.log(newStatus);
        window.store.dispatch({type: 'CHANGE_STATUS', payload: {status: newStatus}});
    }

    goog.events.listen($('attendance-change-button'), goog.events.EventType.CLICK, changeStatus);


    function render() {
        var currentState = window.store.getState();
        $('attendance-position-title').innerText = currentState.position.position;
    }

    render();
    window.store.subscribe(render);
};
