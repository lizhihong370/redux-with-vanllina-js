goog.provide('demo.redux2.general');

demo.redux2.general = function () {

};

demo.redux2.general.prototype.init = function () {
    function render() {
        var currentState = window.store.getState();
        $('general-position-title').innerText = currentState.position.position;
        $('general-status').innerText = currentState.status.status;
    }

    render();
    window.store.subscribe(render);
};

