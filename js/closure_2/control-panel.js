goog.provide('demo.redux2.control');

demo.redux2.control = function () {

};

demo.redux2.control.prototype.reducer = function () {
    return function position(state, action) {

        if (typeof state === 'undefined') {
            return {position: 'Developer'}
        }

        switch (action.type) {
            case 'CHANGE_POSITION':
                return Object.assign({}, state, {position: action.payload.position});
            default:
                return state
        }
    }
};

demo.redux2.control.prototype.init = function () {

    function changePosition() {
        let newPosition = $('position-select').value;
        console.log(newPosition);
        window.store.dispatch({type: 'CHANGE_POSITION', payload: {position: newPosition}});
    }

    goog.events.listen($('position-change-button'), goog.events.EventType.CLICK, changePosition);

    function render() {
        var currentState = window.store.getState();
        $('control-position-title').innerText=currentState.position.position;
    }

    render();
    window.store.subscribe(render);
};




