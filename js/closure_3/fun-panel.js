goog.provide('demo.redux3.fun');

demo.redux3.fun = function () {

};


demo.redux3.fun.prototype.reducer = function () {
    return function fun(state, action) {

        if (typeof state === 'undefined') {
            return {isFetching: false, content: ''}
        }

        switch (action.type) {
            case 'FETCH_REQUEST':
                return Object.assign({}, state,
                    {
                        isFetching: true
                    });
            case 'GET_FUN':
                console.log(action.payload.content);
                return Object.assign({}, state,
                    {
                        isFetching: false,
                        content: action.payload.content.data
                    });
            default:
                return state;
        }
    }
};

demo.redux3.fun.prototype.init = function () {

    function fetchAction() {
        return function (dispatch) {
            dispatch({type: 'FETCH_REQUEST'});
            return fetch("http://127.0.0.1:8088/hello",{ method: 'GET', mode: 'cors'})
                .then(function (response) {
                    response.json().then(function (data) {
                        dispatch({type:'GET_FUN',payload: {content:data }})
                    })

                });
        }
    }

    function fetchFun() {
        window.store.dispatch(fetchAction());
    }

    goog.events.listen($('fetch-fun'), goog.events.EventType.CLICK, fetchFun);


    function render() {
        var currentState = window.store.getState();
        if (currentState.fun.isFetching){
            $('loading-text').style.display="block";
            $('content').innerText = '';
        } else{
            $('loading-text').style.display="none";
            $('content').innerText = currentState.fun.content;
        }

    }

    render();
    window.store.subscribe(render);
};
